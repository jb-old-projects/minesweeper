package com.biskup.minesweeperservlets.controller;

import com.biskup.minesweeperservlets.models.Game;
import com.biskup.minesweeperservlets.models.Minefield;
import com.biskup.minesweeperservlets.models.Scoreboard;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author Jakub Biskup
 * @version 4.0
 */ 
public class MinesweeperGame extends HttpServlet {

    /**
     * Pointer to game instance.
     */
    private Game game;
    
    /**
     * Pointer to scoreboard instance.
     */
    private Scoreboard scoreboard = new Scoreboard();

    /**
     * Pointer to minefield.
     */
    private Minefield minefield;
    
    /**
     * Scoreboard getter.
     * @return pointer to scoreboard.
     */
    public Scoreboard getScoreboard() {
        return scoreboard;
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            RequestDispatcher dis = null;
            
            if(request.getParameter("restartGameButton") != null)
            {
                game.restartGame();
                minefield = game.getMinefield();
                dis = getServletContext().getRequestDispatcher("/index.jsp");
            }
            
            Cookie[] cookies = request.getCookies();
            
            int gameStartedCount = 0;
            int gameOverCount = 0;
            
            if(cookies != null)
                {
                    for(Cookie item: cookies)
                    {
                        if(item.getName().equals("gameStartedCount"))
                        {
                           gameStartedCount = Integer.parseInt(item.getValue());
                        }
                        else if(item.getName().equals("gameOverCount"))
                        {
                           gameOverCount = Integer.parseInt(item.getValue());
                        }
                    }
                }
            
            switch(request.getParameter("actionID"))
            {
                case "startGame":
                    gameStartedCount++;
                    game = new Game(request.getParameter("nickname"), scoreboard);
                    minefield = game.getMinefield();
                    dis = getServletContext().getRequestDispatcher("/minesweepergame.jsp");
                    Cookie c1 = new Cookie("gameStartedCount", Integer.toString(gameOverCount));
                    response.addCookie(c1);
                    break;
                case "gameLast":
                    Minefield tmpMinefield = minefield;
                    onCellClicked(request);
                    
                    if(tmpMinefield == minefield)
                    {
                        dis = getServletContext().getRequestDispatcher("/minesweepergame.jsp");
                    }
                    else
                    {
                        dis = getServletContext().getRequestDispatcher("/index.jsp");  
                    }
                    break;
                case "gameOver":
                    gameOverCount++;
                    game.restartGame();
                    minefield = game.getMinefield();
                    dis = getServletContext().getRequestDispatcher("/index.jsp");
                    Cookie c2 = new Cookie("gameOverCount", Integer.toString(gameOverCount));
                    response.addCookie(c2);
                    break;
            }
                                
            HttpSession active = request.getSession(true);        
            
            active.setAttribute("minefield", minefield);
            active.setAttribute("game", game);
            active.setAttribute("actionType", game.getActionType());
            active.setAttribute("scoreboard", scoreboard);
                        
            dis.forward(request, response);
        }
    }
    
    /**
     * Method which is called after click on any cell.
     * @param request request information.
     */
    private void onCellClicked(HttpServletRequest request)
    {       
        String key = "";
        
        request.getParameterMap().keySet().forEach(x -> {
                if(x.contains("button"))
                {
                    String dividedData[] = x.split(":");
                    String coords[] = dividedData[1].split(",");
                    Integer posX = Integer.parseInt(coords[0]);
                    Integer posY = Integer.parseInt(coords[1]);
                    
                    String mouseButton = "";
        
                    if(request.getParameter("actionType").equals("OpenCell"))
                    {
                        mouseButton = "PRIMARY";
                        game.setActionType(Game.ActionType.OpenCell);
                    }
                    else
                    {
                        mouseButton = "SECONDARY";
                        game.setActionType(Game.ActionType.MarkFlag);
                    }
                    
                    game.getMinefield().getMinefield().get(posX).get(posY).onMouseClickAction(mouseButton);
                }
                else if(x.contains("restartGameButton"))
                {
                    game.restartGame();
                    minefield = game.getMinefield();
                }
                    });
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
