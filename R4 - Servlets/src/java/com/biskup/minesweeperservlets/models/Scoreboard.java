package com.biskup.minesweeperservlets.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Jakub Biskup
 * @version 4.0
 */
public class Scoreboard 
{
    /**
     * Scoreboard elements list.
     */
    private List<ScoreboardElement> scoreboard = new ArrayList<>();

    /**
     * Scoreboard list getter.
     * @return scoreboard.
     */
    public List<ScoreboardElement> getScoreboardList()
    {
        return scoreboard;
    }
        
    /**
     * Method which is adding score info to scoreboard list.
     * @param scoreInfo 
     */
    public void addUserScore(ScoreboardElement scoreInfo)
    {
        if(scoreInfo != null)
        {
            scoreboard.add(scoreInfo); 
            sortScoreboardList();
        }
    }
    
    /**
     * Method which is sorting scoreboard list.
     */
    private void sortScoreboardList()
    {
       Collections.sort(scoreboard, (a,b) ->
       {
           int scoreA = a.getScore();
           int scoreB = b.getScore();
           
           return scoreB - scoreA;
       });
    }
}
