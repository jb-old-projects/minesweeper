package com.biskup.minesweeperservlets.models;

/**
 * @author Jakub Biskup
 * @version 4.0
 */

public class Game implements GameValues, GameFlow
{
    /**
     * Pointer to minefield.
     */
    protected Minefield minefield;
    
    /**
     * Action type which can be done in game.
     */
    private ActionType actionType = ActionType.OpenCell;
    
    /**
     * Pointer to scoreboard.
     */
    private Scoreboard scoreboard;

    /**
     * Action type getter.
     * @return action type value.
     */
    @Override
    public ActionType getActionType() {
        return actionType;
    }

    /**
     * Action type setter.
     * @param actionType action type value.
     */
    @Override
    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    /**
     * Minefield getter.
     * @return pointer to minefield.
     */
    public Minefield getMinefield() {
        return minefield;
    }

    /**
     * Current game state
     */
    private GameState gameState;

    /**
     * Determines if user clicked first cell
     */
    private boolean firstCellOpened = false;
    
    /**
     * Current player score.
     */
    private int score = 0;
    
    /**
     * Current player nickname.
     */
    private String nickname;
    
    /**
     * Score getter.
     * @return score value.
     */
    @Override
    public int getScore() {
        return score;
    }

    /**
     * Score setter.
     * @param score sets score value.
     */
    @Override
    public void setScore(int score) {
        this.score = score;
    }
    
    /**
     * First cell opened getter.
     * @return first cell opened value.
     */
    @Override
    public boolean getFirstCellOpened() {
            return firstCellOpened;
    }

    /**
     * Game state getter.
     * @return game state value.
     */
    @Override
    public GameState getGameState() {
        return gameState;
    }

    /**
     * First cell opened setter.
     * @param firstCellOpened sets first cell opened value.
     */
    @Override
    public void setFirstCellOpened(boolean firstCellOpened)
    {
        this.firstCellOpened = firstCellOpened;
    }

    /**
     * Game state setter.
     * @param gameState sets game state value.
     */
    @Override
    public void setGameState(GameState gameState)
    {
        this.gameState = gameState;
    }

    /**
     * Nickname getter.
     * @return nickname value.
     */
    @Override
    public String getNickname() {
        return nickname;
    }

    /**
     * 
     * @param nickname 
     */
    @Override
    public void setNickname(String nickname) {
       this.nickname = nickname;
    }

    public enum GameState
    {
        Game,
        GameOver
    }
    
    public enum ActionType
    {
        OpenCell,
        MarkFlag
    }

    /**
     * Game class contructor which is calling gameInit method.
     * @param nickname user nickname.
     * @param scoreboard pointer to scoreboard instance.
     */

    public Game(String nickname, Scoreboard scoreboard)
    {
        this.scoreboard = scoreboard;
        
        if(nickname.equals(""))
        {
            String userID = java.util.UUID.randomUUID().toString();
            userID = userID.substring(0, 5);
            this.nickname = "user$" + userID;
        }
        else
        {
            this.nickname = nickname;
        }
        
        gameInit();
    }

    /**
     *  Restores default game values and cleares minefield.
     */

    @Override
    public void restartGame()
    {
        scoreboard.addUserScore(new ScoreboardElement(nickname, score));
        
        minefield = new Minefield(this,this, this);
        initDefaultValues();
    }

    /**
     * Game initializing method
     */

    private void gameInit()
    {
        minefield = new Minefield(this, this, this);
        initDefaultValues();
    }

    /**
     * Method which is initializing default game values
     */

    private void initDefaultValues()
    {
        gameState = GameState.Game;
        firstCellOpened = false;
        actionType = ActionType.OpenCell;
        score = 0;
    }
}
