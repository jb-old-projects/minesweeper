<%-- 
    Document   : minesweeperscoreboard
    Created on : 2018-01-18, 17:09:52
    Author     : Jakub Biskup
--%>

<%@page import="com.biskup.minesweeperservlets.models.ScoreboardElement"%>
<%@page import="com.biskup.minesweeperservlets.models.Scoreboard"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Minesweeper Scoreboard</title>
    </head>
    <body>
        <h1 style="margin:0 auto;display:table">
            <img src="https://cdn.instructables.com/FK1/W8ZX/3ESEXCFMJJO/FK1W8ZX3ESEXCFMJJO.MEDIUM.jpg"
              style="width:50px;height:50px;border:0;display:block;margin:0 auto;">
            <h2 style="margin:10px auto;display:table">SCOREBOARD</h2>
        <table style="margin:5px auto;display:table;border:1px solid black;">
            <tr>
                <th style="border: 1px solid black;padding: 5px;text-align: center;">Nickname</th>
                <th style="border: 1px solid black;padding: 5px;text-align: center;">Score</th>
            </tr>
        <%
                HttpSession active = request.getSession(true);
                        
                Scoreboard scoreboard = (Scoreboard)active.getAttribute("scoreboard");
                
                if(scoreboard != null)
                {
                    for(int i = 0 ; i < scoreboard.getScoreboardList().size(); i++)
                    {
                        out.println("<tr>");
                        ScoreboardElement elem = scoreboard.getScoreboardList().get(i);

                        String nickname = elem.getNickname();
                        int score = elem.getScore();
                        
                        out.println("<td style=\"border: 1px solid black;padding: 5px;text-align: center;\">" + nickname + "</td>");
                        out.println("<td style=\"border: 1px solid black;padding: 5px;text-align: center;\">" + score + "</td>");
                        out.println("</tr>");
                    }
                }
            %>
        </table>
        <h3 style="margin:20px auto;display:table">STATISTICS</h3>
        <h4 style="margin:5px auto;display:table">
         <%
                   int gameStartedCount = 0;
                   int gameOverCount = 0;

                   Cookie[] cookies = request.getCookies();
                   if(cookies != null)
                   {
                       for(Cookie item: cookies)
                       {
                           if(item.getName().equals("gameStartedCount"))
                           {
                              gameStartedCount = Integer.parseInt(item.getValue());
                           }
                           else if(item.getName().equals("gameOverCount"))
                           {
                              gameOverCount = Integer.parseInt(item.getValue());
                           }
                       }
                   }
                   
                   out.println("Game started count: " + gameStartedCount);
                   out.println("<br>");
                   out.println("Game over count: " + gameOverCount);
             %>
        </h4>
        </h1>
    </body>
</html>
