package com.biskup.minesweeperservlets.models;

import org.junit.Test;
import org.junit.Assert;

/**
 * @author Jakub Biskup
 * @version 4.0
 */
public class ScoreboardTest
{
    @Test
    public void addUserScoreTest()
    {
        // given
        Scoreboard scoreboard = new Scoreboard();
        Scoreboard scoreboard2 = new Scoreboard();
        
        // when
        scoreboard.addUserScore(new ScoreboardElement("x", 1));
        scoreboard2.addUserScore(null);

        // then
        Assert.assertEquals("x" ,scoreboard.getScoreboardList().get(0).nickname);
        Assert.assertEquals(1 ,scoreboard.getScoreboardList().get(0).score);
        Assert.assertEquals(0, scoreboard2.getScoreboardList().size());
    }
}
