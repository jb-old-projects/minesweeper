package com.biskup.minesweeperservlets.models;

import org.junit.Test;
import org.junit.Assert;

import java.util.List;

public class MinefieldTest
{
    @Test
    public void createEmptyMinefieldTest()
    {
        // given
        Scoreboard scoreboard = new Scoreboard();
        Game game = new Game("xd", scoreboard);
        Minefield minefieldObj1 = new Minefield(game, game, game);
        Minefield minefieldObj2 = new Minefield(game, game, game);

        // when
        minefieldObj1.createMinefieldContent(new MinefieldCell(minefieldObj1, 0,0, false));
        minefieldObj2.createMinefieldContent(null);

        // then
        Assert.assertNotNull(minefieldObj1.neighborsPoints);
        Assert.assertNotNull(minefieldObj2.neighborsPoints);
    }

    @Test
    public void gettingCellNeighborsTest()
    {
        // given
        Scoreboard scoreboard = new Scoreboard();
        Game game = new Game("xd", scoreboard);

        Minefield minefieldObj1 = new Minefield(game, game, game);
        minefieldObj1.createMinefieldContent(new MinefieldCell(minefieldObj1, 0,0, false));

        Minefield minefieldObj2 = new Minefield(game, game, game);
        minefieldObj2.createMinefieldContent(null);

        // when
        List<MinefieldCell> list1 = minefieldObj1.getCellNeighbors(null);
        List<MinefieldCell> list2 = minefieldObj1.getCellNeighbors(minefieldObj1.minefield.get(0).get(0));
        List<MinefieldCell> list3 = minefieldObj1.getCellNeighbors(minefieldObj1.minefield.get(1).get(1));
        List<MinefieldCell> list4 = minefieldObj1.getCellNeighbors(minefieldObj1.minefield.get(2).get(2));

        // then
        Assert.assertNotNull(list1);
        Assert.assertEquals(0, list1.size());
        Assert.assertEquals(3, list2.size());
        Assert.assertEquals(8, list3.size());
        Assert.assertEquals(minefieldObj1.minefield.get(0).get(0), list3.get(0));
        Assert.assertEquals(8, list4.size());
        Assert.assertEquals(minefieldObj1.minefield.get(1).get(1), list4.get(0));
    }

    @Test
    public void minefieldConstructorCreatesEmptyMinefieldTest()
    {
        // given
        Scoreboard scoreboard = new Scoreboard();
        Game game = new Game("xd", scoreboard);

        // when
        Minefield minefieldObj = new Minefield(game, game, game);

        // then
        for(List<MinefieldCell> row : minefieldObj.minefield)
        {
            for(MinefieldCell cell : row)
            {
                Assert.assertNotNull(cell);
            }
        }
    }
}
