package com.biskup.minesweeperservlets.models;

import org.junit.Test;
import org.junit.Assert;

/**
 * @author Jakub Biskup
 * @version 4.0
 */
public class GameTest
{
    @Test
    public void gameRestartingTest()
    {
        // given
        Game game = new Game("xd", null);
        Minefield minefieldObj = new Minefield(game, game, game);
        game.minefield = minefieldObj;

        game.setFirstCellOpened(true);
        game.setGameState(Game.GameState.GameOver);

        // when
        game.restartGame();

        // then
        Assert.assertEquals(false, game.getFirstCellOpened());
        Assert.assertEquals(Game.GameState.Game, game.getGameState());
    }
}
