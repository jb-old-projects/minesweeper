<%-- 
    Document   : index
    Created on : 2018-01-08, 18:47:16
    Author     : Jakub Biskup
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Minesweeper</title>
    </head>
    <body>
        <h1>
            <img src="https://cdn.instructables.com/FK1/W8ZX/3ESEXCFMJJO/FK1W8ZX3ESEXCFMJJO.MEDIUM.jpg"
                 style="width:50px;height:50px;border:0;display:block;margin:0 auto;">
            <form name="MinesweeperGame" action="MinesweeperGame" method="POST">
                <input style="display:block;margin:15px auto;" placeholder="Enter your nickname.." type="text" id="nickname" name="nickname" value="" size="25" />
                <br>
                <input style="display:block;margin:-30px auto;" type="submit" value="Start game" name="startGameButton" />
                <input type="hidden" name="actionID" value="startGame" />
            </form>
            <form name="MinesweeperScoreboard" action="MinesweeperScoreboard" method="POST">
                <input style="display:block;margin:40px auto;" type="submit" value="Scoreboard" name="scoreboard" />
                <input type="hidden" name="actionID" value="getScoreboard" />
            </form>
        </h1>
    </body>
</html>
