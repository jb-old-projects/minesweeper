<%-- 
    Document   : minesweepergame
    Created on : 2018-01-08, 18:47:16
    Author     : Jakub Biskup
--%>

<%@page import="com.biskup.minesweeperservlets.models.Game"%>
<%@page import="com.biskup.minesweeperservlets.models.MinefieldCell"%>
<%@page import="com.biskup.minesweeperservlets.models.Minefield"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Minesweeper</title>
    </head>
    <body>
        <h1>
            <form name="MinesweeperGame" action="MinesweeperGame" method="POST">
                <input type="image" name="restartGameButton" src="https://cdn.instructables.com/FK1/W8ZX/3ESEXCFMJJO/FK1W8ZX3ESEXCFMJJO.MEDIUM.jpg"
                 style="width:50px;height:50px;border:0;display:block;margin:0 auto;">
                <div style="margin:15px auto;display:table;">                    
                    <%
                        HttpSession active = request.getSession(true);
                        
                        Game game = (Game)active.getAttribute("game");
                        
                        Game.ActionType actionType = (Game.ActionType)active.getAttribute("actionType");
                    
                        if(actionType.equals(Game.ActionType.OpenCell))
                        {
                            out.println(
                                    "<label>" +
                                    "<input checked type=\"radio\" name=\"actionType\" value=\"OpenCell\"/>" +
                                    "⛏" +
                                    "</label>" +
                                    "<label>" +
                                    "<input type=\"radio\" name=\"actionType\" value=\"MarkFlag\"/>" +
                                    "🚩" +
                                    "</label>"
                            );
                        }
                        else if(actionType.equals(Game.ActionType.MarkFlag))
                        {
                            out.println(
                                    "<label>" +
                                    "<input type=\"radio\" name=\"actionType\" value=\"OpenCell\"/>" +
                                    "⛏" +
                                    "</label>" +
                                    "<label>" +
                                    "<input checked type=\"radio\" name=\"actionType\" value=\"MarkFlag\"/>" +
                                    "🚩" +
                                    "</label>"
                            );
                        }
                        %>                    
                </div>
                <br>
                <table style="margin:-45px auto;display:table;">
                    <%
                        Minefield minefield;

                        minefield = (Minefield)active.getAttribute("minefield");  
                                               
                        for(int i = 0 ; i < minefield.getMinefield().size() ; i++)
                        {
                            out.println("<tr>");
                            for(int j = 0 ; j < minefield.getMinefield().get(i).size() ; j++)
                            {
                                out.println("<td>");
                                String buttonID = "[" + String.valueOf(i) + "," + String.valueOf(j) + "]";
                                
                                MinefieldCell cell = minefield.getMinefield().get(i).get(j);
                                
                                out.println(cell.getCellHtmlString());
                                
                                out.println("</td>");
                            }
                            out.println("</tr>");
                        }
                        
                        if(game.getGameState() == Game.GameState.Game)
                        {
                            out.println("<input type=\"hidden\" name=\"actionID\" value=\"gameLast\" />");
                        }
                        else
                        {
                            out.println("<input type=\"hidden\" name=\"actionID\" value=\"gameOver\" />");
                        }
                    %>
                </table>
            </form>
        </h1>
    </body>
</html>
