package com.biskup.minesweeperservlets.controller;

import com.biskup.minesweeperservlets.models.ScoreboardElement;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 * Scoreboard database class.
 * @author Jakub Biskup
 * @version 5.0
 */
public class DatabaseController {
    
    /**
     * Statement which is necessary to execute commands in database.
     */
    private Statement statement;
       
    /**
     * Variable which is determining if there is any connection with database.
     */
    private boolean isConnected = false;

    /**
     * Is connected getter.
     * @return is connected value.
     */
    public boolean isConnected() {
        return isConnected;
    }
    
    /**
     * Database connection method.
     */
    public void connectToDatabase()
    {
        Connection conection = null;
        try
        {
            Context cntx = new InitialContext();
            String dbaddress = (String)cntx.lookup("java:comp/env/ScoreboardDatabaseAddress");
            String dblogin = (String)cntx.lookup("java:comp/env/ScoreboardDatabaseLogin");
            String dbpassword = (String)cntx.lookup("java:comp/env/ScoreboardDatabasePassword");
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            conection = DriverManager.getConnection(dbaddress, dblogin, dbpassword);
            statement = conection.createStatement();
        }
        catch(ClassNotFoundException ex)
        {
            
        }
        catch(SQLException ex)
        {
            
        }
        catch(NamingException ex)
        {
            
        }
        
        isConnected = true;
    }
    
    /**
     * Table creating method.
     */
    public void createDatabase()
    {
        try
        {
            statement.executeUpdate("CREATE TABLE ScoreboardDatabase (nickname varchar(33), score int)");
        }
        catch(SQLException ex)
        {

        }
    }
    
    /**
     * Records adding method.
     * @param nickname current user nickname
     * @param score current user score
     */
    public void addRecord(String nickname, int score)
    {
        try
        {
            statement.executeUpdate("INSERT INTO ScoreboardDatabase VALUES ('" + nickname + "', " + score + ")");
        }
        catch(SQLException ex)
        {
            
        }
    }
    
    /**
     * Method which is returning scoreboards records.
     * @return list of scoreboards elements.
     */
    public List<ScoreboardElement> getScoreboardRecords()
    {
        List<ScoreboardElement> dbList = new ArrayList<>();
        try
        { 
            ResultSet rs = statement.executeQuery("SELECT * FROM ScoreboardDatabase ORDER BY score DESC");
            while(rs.next())
            {
                String nickname = rs.getString("nickname");
                int score = rs.getInt("score");

                ScoreboardElement elem = new ScoreboardElement(nickname, score);
                
                dbList.add(elem);
            }
            rs.close();
        }
        catch(SQLException ex)
        {
            
        }
        
        return dbList;
    }
}
