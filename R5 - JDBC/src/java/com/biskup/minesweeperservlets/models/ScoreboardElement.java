package com.biskup.minesweeperservlets.models;

/**
 * @author Jakub Biskup
 * @version 5.0
 */
public class ScoreboardElement 
{
    /**
     * User nickname.
     */
    String nickname;
 
    /**
     * User score.
     */
    int score;

    /**
     * Nickname getter.
     * @return nickname value.
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Score getter.
     * @return score value.
     */
    public int getScore() {
        return score;
    }
    
    /**
     * ScoreboardElement constructor.
     * @param nickname user nickname.
     * @param score user score.
     */
    public ScoreboardElement(String nickname, int score)
    {
        this.nickname = nickname;
        this.score = score;
    }
}
