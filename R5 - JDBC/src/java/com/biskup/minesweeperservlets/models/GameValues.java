package com.biskup.minesweeperservlets.models;

/**
 * Game values interface which is necessary to separate Game class methods. Contains getters and setters.
 * @author Jakub Biskup
 * @version 5.0
 */

public interface GameValues
{
    boolean getFirstCellOpened();
    Game.GameState getGameState();
    void setFirstCellOpened(boolean firstCellOpened);
    void setGameState(Game.GameState gameState);
    Game.ActionType getActionType();
    void setActionType(Game.ActionType actionType);
    int getScore();
    void setScore(int score);
    String getNickname();
    void setNickname(String nickname);
}
