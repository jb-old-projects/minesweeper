package com.biskup.minesweeper;

import com.biskup.minesweeper.Models.Game;
import javafx.application.Application;

/**
 * @author Jakub Biskup
 * @version 1.1
 */
public class Main
{
    /**
     * Main class of application
     * @param args Starting parametrs (optional)
     */

    public static void main(String[] args)
    {
        Application.launch(Game.class, args);
    }
}
