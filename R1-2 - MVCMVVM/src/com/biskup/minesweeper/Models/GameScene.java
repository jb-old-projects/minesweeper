package com.biskup.minesweeper.Models;

import com.biskup.minesweeper.Views.GameSceneView;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GameScene
{
    /**
     * Pointer to game scene view
     */
    private GameSceneView gameSceneView;

    /**
     * Current scene
     */
    private Scene scene;

    /**
     * Current stage
     */
    private Stage stage;

    /**
     * Pointer to main root
     */
    private Pane paneRoot;

    public Stage getStage()
    {
        return stage;
    }

    public Pane getPaneRoot()
    {
        return paneRoot;
    }

    /**
     * Game Scene contructor
     * @param width Scene width
     * @param height Scene height
     * @param stage Scene stage
     */

    public GameScene(int width, int height, Stage stage)
    {
        scene = new Scene(createScene(width, height));

        this.stage = stage;
        this.stage.setScene(scene);

        gameSceneView = new GameSceneView(stage);
        gameSceneView.showStage();
    }

    /**
     * Method which is creating JavaFX scene
     * @param width Scene width
     * @param height Scene height
     * @return Method is returning pointer to scene root
     */

    private Parent createScene(int width, int height)
    {
        paneRoot = new Pane();
        paneRoot.setPrefSize(width, height);

        return paneRoot;
    }
}
