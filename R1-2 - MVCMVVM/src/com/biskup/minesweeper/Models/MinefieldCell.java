package com.biskup.minesweeper.Models;

import com.biskup.minesweeper.Views.MinefieldCellView;
import javafx.beans.property.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class MinefieldCell extends StackPane
{
    /**
     * Pointer to minefield model variable
     */
    private Minefield minefield;

    /**
     * Pointer to minefield cell view variable
     */
    private MinefieldCellView minefieldCellView;

    /**
     * Cell X-Axis position
     */
    private int posX;

    /**
     * Cell Y-Axis position
     */
    private int posY;

    /**
     * Determines if cell contains bomb
     */
    private BooleanProperty containsBomb = new SimpleBooleanProperty();

    /**
     * Cell neighbors cell with bomb count
     */
    private IntegerProperty bombsNear = new SimpleIntegerProperty();

    /**
     * Determines if cell is marked
     */
    private BooleanProperty marked = new SimpleBooleanProperty(false);

    /**
     * Determines if cell was opened
     */
    private BooleanProperty wasOpened = new SimpleBooleanProperty(false);

    /**
     * Cell width
     */
    private int width = 40;

    /**
     * Cell height
     */
    private int height = 40;

    private BooleanProperty isTextVisible = new SimpleBooleanProperty(false);
    private StringProperty textString = new SimpleStringProperty("");
    private ObjectProperty<Color> cellFillColor = new SimpleObjectProperty<>();
    private ObjectProperty<Color> textColor = new SimpleObjectProperty<>();

    public boolean isIsTextVisible() {
        return isTextVisible.getValue();
    }

    void setBombsNear(int bombsNear)
    {
        this.bombsNear.setValue(bombsNear);
    }

    boolean containsBomb()
    {
        return containsBomb.getValue();
    }

    int getPosX()
    {
        return posX;
    }

    int getPosY()
    {
        return posY;
    }

    void setContainsBomb(boolean containsBomb)
    {
        this.containsBomb.setValue(containsBomb);
    }

    public boolean isTextVisible() {
        return isTextVisible.getValue();
    }

    public String getTextString() {
        return textString.getValue();
    }

    public Color getCellFillColor() {
        return cellFillColor.getValue();
    }

    void setCellFillColor(Color cellFillColor){
        this.cellFillColor.setValue(cellFillColor);
    }

    public Color getTextColor() {
        return textColor.getValue();
    }

    void setTextColor(Color textColor){
        this.textColor.setValue(textColor);
    }

    BooleanProperty getTextVisibleProperty() {
        return isTextVisible;
    }

    StringProperty getTextStringProperty() {
        return textString;
    }

    ObjectProperty<Color> getStrokeColorProperty() {
        return cellFillColor;
    }

    ObjectProperty<Color> getCellFillProperty() {
        return textColor;
    }

    public void refreshView()
    {
        minefieldCellView.refresh(this);
    }

    /**
     * MinefieldCell contructor which is setting up Minefield Cell values
     * @param minefield Pointer to minefield object (it is necessary to have access to Game interafces)
     * @param posX X Axis position of cell
     * @param posY Y Axis position of cell
     * @param containsBomb Determines if cell contains bomb
     */

    MinefieldCell(Minefield minefield, int posX, int posY, boolean containsBomb)
    {
        this.minefield = minefield;

        this.posX = posX;
        this.posY = posY;
        this.containsBomb.setValue(containsBomb);

        minefieldCellView = new MinefieldCellView(getChildren(), width, height);

        cellFillColor.setValue(Color.BLACK);
        isTextVisible.setValue(false);
        textColor.setValue(Color.RED);

        setTranslateX(posX * width);
        setTranslateY(posY * height);

        setOnMouseClicked(this::onMouseClickAction);
    }

    /**
     * Method which is called after click on the cell
     * @param event Variable which is describing event which occured
     */

    private void onMouseClickAction(MouseEvent event)
    {
        MouseButton button = event.getButton();

        System.out.println(button);

        if(minefield.getGameValues().getGameState() == Game.GameState.GameOver)
        {
            minefield.getGameFlow().restartGame();
            return;
        }

        if(!minefield.getGameValues().getFirstCellOpened())
        {
            if(button == MouseButton.SECONDARY)
            {
                return;
            }
            else if(button == MouseButton.PRIMARY)
            {
                minefield.createMinefieldContent(this);
                minefield.getGameValues().setFirstCellOpened(true);
            }
        }

        if(!wasOpened.getValue())
        {
            if(button == MouseButton.PRIMARY && !marked.getValue())
            {
                try
                {
                    isTextVisible.setValue(true);

                    if (containsBomb.getValue())
                    {
                        UnlockBombCell();
                    }

                    else
                    {
                        UnlockNormalCell(event);
                    }
                }
                catch (GameOverException ex)
                {
                    ex.Show();
                }
            }
            else if(button == MouseButton.SECONDARY)
            {
                setCellMarked(!marked.getValue());
            }
        }
    }

    /**
     * Method which is changing cell state
     * @param marked Variable which is describing if variable is marked or not
     */

    private void setCellMarked(boolean marked)
    {
        this.marked.setValue(marked);

        isTextVisible.setValue(marked);

        if(marked)
        {
            textColor.setValue(Color.RED);
            textString.setValue("!");
        }
        else
        {
            textColor.setValue(Color.BLACK);

            if(bombsNear.getValue() > 0)
            {
                textString.setValue(String.valueOf(bombsNear.getValue()));
            }
            else
            {
                textString.setValue("");
            }
        }
    }

    /**
     * Method which is handling normal cell unlock event
     * @param event Variable which is describing event which occured
     */

    private void UnlockNormalCell(MouseEvent event)
    {
        wasOpened.setValue(true);
        textColor.setValue(Color.BLACK);
        isTextVisible.setValue(true);
        cellFillColor.setValue(null);

        if(bombsNear.getValue() > 0)
        {
            textString.setValue(String.valueOf(bombsNear.getValue()));
        }
        else
        {
            textString.setValue("");
        }

        if(textString.getValue().isEmpty())
        {
            minefield.getCellNeighbors(this).forEach(neighbor -> neighbor.onMouseClickAction((event)));
        }
    }

    /**
     * Method which is handling bomb cell unlock event
     * @throws GameOverException Exception which is thrown after unlocking bomb cell
     */

    private void UnlockBombCell() throws GameOverException
    {
        textString.setValue("X");
        minefield.getGameValues().setGameState(Game.GameState.GameOver);
        throw new GameOverException(this);
    }
}
