package com.biskup.minesweeper.Models;

import com.biskup.minesweeper.Views.GameView;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class Game extends Application implements GameValues, GameFlow
{
    /**
     * Pointer to minefield
     */
    Minefield minefield;

    /**
     * Pointer to game view
     */
    private GameView gameView;

    /**
     * Current game state
     */
    private GameState gameState;

    /**
     * Determines if user clicked first cell
     */
    private boolean firstCellOpened = false;

    @Override
    public boolean getFirstCellOpened()
    {
        return firstCellOpened;
    }

    @Override
    public GameState getGameState()
    {
        return gameState;
    }

    @Override
    public void setFirstCellOpened(boolean firstCellOpened)
    {
        this.firstCellOpened = firstCellOpened;
    }

    @Override
    public void setGameState(GameState gameState)
    {
        this.gameState = gameState;
    }

    @Override
    public void start(Stage stage) throws Exception
    {
        gameInit(stage);
    }

    /**
     *  Restores default game values and clears minefield.
     */

    public void restartGame()
    {
        minefield = new Minefield(this,this, this);

        if(gameView != null)
        {
            gameView.onGameRestartAction(minefield.getMinefield());
        }

        initDefaultValues();
    }

    void refreshView(MinefieldCell cell)
    {
        gameView.refreshView(cell);
    }

    public enum GameState
    {
        Game,
        GameOver
    }

    /**
     * Game initializing method
     * @param stage Pointer to stage which will be used in updating view
     */

    private void gameInit(Stage stage)
    {
        minefield = new Minefield(this,this, this);
        gameView = new GameView(stage, minefield.getMinefield());
        initDefaultValues();
    }

    /**
     * Method which is initializing default game values
     */

    private void initDefaultValues()
    {
        gameState = GameState.Game;
        firstCellOpened = false;
    }
}