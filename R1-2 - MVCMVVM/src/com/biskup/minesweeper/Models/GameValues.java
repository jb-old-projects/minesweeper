package com.biskup.minesweeper.Models;

/**
 * Game values interface which is necessary to separate Game class methods. Contains getters and setters.
 * @author Jakub Biskup
 * @version 1.1
 */

public interface GameValues
{
    public boolean getFirstCellOpened();
    public Game.GameState getGameState();
    public void setFirstCellOpened(boolean firstCellOpened);
    public void setGameState(Game.GameState gameState);
}
