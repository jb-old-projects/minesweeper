package com.biskup.minesweeper.Models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class which contains Game class methods tests.
 * @author Jakub Biskup
 * @version 1.1
 */

class GameTest
{
    @Test
    void gameRestartingTest()
    {
        // given
        Game game = new Game();
        game.minefield = new Minefield(game, game, game);

        game.setFirstCellOpened(true);
        game.setGameState(Game.GameState.GameOver);

        // when
        game.restartGame();

        // then
        Assertions.assertEquals(false, game.getFirstCellOpened());
        Assertions.assertEquals(Game.GameState.Game, game.getGameState());
    }
}