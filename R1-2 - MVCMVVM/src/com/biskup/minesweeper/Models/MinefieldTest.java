package com.biskup.minesweeper.Models;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * Class which contains Minefield class methods tests.
 * @author Jakub Biskup
 * @version 1.1
 */

class MinefieldTest
{
    @Test
    void creatingMinefieldContentTest()
    {
        // given
        Game game = new Game();
        Minefield minefieldObj1 = new Minefield(game, game, game);
        Minefield minefieldObj2 = new Minefield(game, game, game);

        // when
        minefieldObj1.createMinefieldContent(new MinefieldCell(minefieldObj1, 0,0, false));
        minefieldObj2.createMinefieldContent(null);

        // then
        Assertions.assertNotNull(minefieldObj1.neighborsPoints);
        Assertions.assertNotNull(minefieldObj2.neighborsPoints);
    }

    @Test
    void gettingCellNeighborsTest()
    {
        // given
        Game game = new Game();

        Minefield minefieldObj1 = new Minefield(game, game, game);
        minefieldObj1.createMinefieldContent(new MinefieldCell(minefieldObj1, 0,0, false));

        Minefield minefieldObj2 = new Minefield(game, game, game);
        minefieldObj2.createMinefieldContent(null);

        // when
        List<MinefieldCell> list1 = minefieldObj1.getCellNeighbors(null);
        List<MinefieldCell> list2 = minefieldObj1.getCellNeighbors(minefieldObj1.minefield.get(0).get(0));
        List<MinefieldCell> list3 = minefieldObj1.getCellNeighbors(minefieldObj1.minefield.get(1).get(1));
        List<MinefieldCell> list4 = minefieldObj1.getCellNeighbors(minefieldObj1.minefield.get(2).get(2));

        // then
        Assertions.assertNotNull(list1);
        Assertions.assertEquals(0, list1.size());
        Assertions.assertEquals(3, list2.size());
        Assertions.assertEquals(8, list3.size());
        Assertions.assertEquals(minefieldObj1.minefield.get(0).get(0), list3.get(0));
        Assertions.assertEquals(8, list4.size());
        Assertions.assertEquals(minefieldObj1.minefield.get(1).get(1), list4.get(0));
    }

    @Test
    void minefieldConstructorCreatesEmptyMinefieldTest()
    {
        // given
        Game game = new Game();

        // when
        Minefield minefieldObj = new Minefield(game, game, game);

        // then
        for(List<MinefieldCell> row : minefieldObj.minefield)
        {
            for(MinefieldCell cell : row)
            {
                Assertions.assertNotNull(cell);
            }
        }
    }
}