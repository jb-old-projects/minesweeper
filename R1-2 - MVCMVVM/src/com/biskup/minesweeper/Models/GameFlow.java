package com.biskup.minesweeper.Models;

/**
 * Game flow interface which is necessary to separate Game class methods
 * @author Jakub Biskup
 * @version 1.1
 */

public interface GameFlow
{
    /**
     * Method which is handling game restart
     */
    public void restartGame();
}
