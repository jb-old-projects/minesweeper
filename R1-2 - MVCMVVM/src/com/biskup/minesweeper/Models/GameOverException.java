package com.biskup.minesweeper.Models;

import javafx.scene.paint.Color;

/**
 * @author Jakub Biskup
 * @version 1.1
 */

public class GameOverException extends Exception
{
    /**
     * Pointer to minefield cell view
     */
    private MinefieldCell minefieldCell;

    /**
     * GameOverException constructor
     * @param minefieldCell Pointer to minefield cell
     */
    public GameOverException(MinefieldCell minefieldCell)
    {
        this.minefieldCell = minefieldCell;
    }

    /**
     * Method which is called after unlocking bomb cell
     */
    public void Show()
    {
        minefieldCell.setCellFillColor(Color.RED);
        minefieldCell.setTextColor(Color.WHITE);
    }
}
