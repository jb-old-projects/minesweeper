package com.biskup.minesweeper.Views;

import javafx.stage.Stage;

/**
 * @author Jakub Biskup
 * @version 1.1
 */

public class GameSceneView
{
    private Stage stage;

    /**
     * GameSceneView contructor
     * @param stage Actual stage
     */

    public GameSceneView(Stage stage)
    {
        this.stage = stage;
        this.stage.setResizable(false);
    }

    /**
     * Method which is showing current stage
     */

    public void showStage()
    {
        stage.show();
    }
}
