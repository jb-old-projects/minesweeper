package com.biskup.minesweeper.Views;

import com.biskup.minesweeper.Models.GameScene;
import com.biskup.minesweeper.Models.MinefieldCell;
import javafx.stage.Stage;

import java.util.List;

public class GameView
{
    /**
     * Pointer to game scene
     */
    protected GameScene gameScene;

    private Stage stage;

    public GameView(Stage stage, List<List<MinefieldCell>> minefield)
    {
        this.stage = stage;
        gameScene = new GameScene(800, 800, stage);
        refreshView(minefield);
    }

    public void refreshView(MinefieldCell cell)
    {
        cell.refreshView();
    }

    private void refreshView(List<List<MinefieldCell>> minefield)
    {
        if(gameScene.getPaneRoot().getChildren().size() == 0)
        {
            initializePaneRootElements(minefield);
        }
        else
        {
            refreshPaneRootElements(minefield);
        }
    }

    private void refreshPaneRootElements(List<List<MinefieldCell>> minefield)
    {
        for(List<MinefieldCell> row : minefield)
        {
            for(MinefieldCell cell : row)
            {
                cell.refreshView();
            }
        }
    }

    private void initializePaneRootElements(List<List<MinefieldCell>> minefield)
    {
        for(List<MinefieldCell> row : minefield)
        {
            for(MinefieldCell cell : row)
            {
                gameScene.getPaneRoot().getChildren().add(cell);
            }
        }
    }

    public void onGameRestartAction(List<List<MinefieldCell>> minefield)
    {
        gameScene.getPaneRoot().getChildren().clear();
        initializePaneRootElements(minefield);
    }
}
