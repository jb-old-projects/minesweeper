package com.biskup.minesweeper.Views;

import com.biskup.minesweeper.Models.MinefieldCell;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.shape.Rectangle;

/**
 * @author Jakub Biskup
 * @version 1.1
 */

public class MinefieldCellView
{
    /**
     * Rectangle variable which is describing cell rect
     */
    private Rectangle fieldRect;

    /**
     * Text variable which contains cell text (f.e. bombs count)
     */
    private Text text;

    /**
     * MinefieldCellView contructor
     * @param children
     * @param width Minefield cell width
     * @param height Minefield cell height
     */

    public MinefieldCellView(ObservableList<Node> children, int width, int height)
    {
        fieldRect = new Rectangle(width, height);
        fieldRect.setStroke(Color.GRAY);
        text = new Text();
        children.addAll(fieldRect, text);
    }

    public Rectangle getFieldRect()
    {
        return fieldRect;
    }

    public void refresh(MinefieldCell cell)
    {
        fieldRect.fillProperty().setValue(cell.getCellFillColor());
        text.setVisible(cell.isTextVisible());
        text.fillProperty().setValue(cell.getTextColor());
        text.setText(cell.getTextString());
    }
}
