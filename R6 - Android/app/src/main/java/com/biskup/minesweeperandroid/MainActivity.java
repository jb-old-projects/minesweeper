package com.biskup.minesweeperandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.biskup.minesweeperandroid.model.Scoreboard;

/**
 * Class which is managing start view of application.
 * @author Jakub Biskup
 * @version 6.0
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * which is called after user click on scoreboard button.
     * @param view reference to clicked object view.
     */
    public void onScoreboardButtonClick(View view)
    {
        Intent intent = new Intent(this, ScoreboardActivity.class);
        startActivity(intent);
    }

    /**
     * which is called after user click on start game button.
     * @param view reference to clicked object view.
     */
    public void onStartGameButtonClick(View view)
    {
        Intent intent = new Intent(this, GameActivity.class);
        String nickname = ((TextView)findViewById(R.id.nicknameField)).getText().toString();
        intent.putExtra("userNickname", nickname);
        startActivity(intent);
    }
}
