package com.biskup.minesweeperandroid.model;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * @author Jakub Biskup
 * @version 6.0
 */

public class GameOverException extends Exception
{
    /**
     * Pointer to minefield cell view
     */
    private MinefieldCell minefieldCell;

    /**
     * GameOverException constructor
     * @param minefieldCell Pointer to minefield cell
     */
    public GameOverException(MinefieldCell minefieldCell)
    {
        this.minefieldCell = minefieldCell;
    }

    /**
     * Method which is called after unlocking bomb cell
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void show()
    {
        minefieldCell.setTextString("💣");
    }
}
