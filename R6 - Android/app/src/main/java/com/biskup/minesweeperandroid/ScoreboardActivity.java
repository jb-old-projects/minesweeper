package com.biskup.minesweeperandroid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biskup.minesweeperandroid.model.Scoreboard;
import com.biskup.minesweeperandroid.model.ScoreboardElement;

/**
 * Class which is managing scoreboard view of application.
 * @author Jakub Biskup
 * @version 6.0
 */

public class ScoreboardActivity extends AppCompatActivity {

    /**
     * Pointer to scoreboard instance.
     */
    private static Scoreboard scoreboard = new Scoreboard();

    /**
     * Method which is adding new score to scoreboard.
     * @param nickname user nickname.
     * @param score user score.
     */
    public static void addUserScore(String nickname, int score)
    {
        scoreboard.addUserScore(new ScoreboardElement(nickname, score));
    }

    /**
     * Scoreboard getter.
     * @return pointer to scoreboard instance.
     */
    public static Scoreboard getScoreboard()
    {
        return scoreboard;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);

        Scoreboard scoreboard = (Scoreboard)getIntent().getSerializableExtra("scoreboard");

        refreshView();
    }

    /**
     * Method which is refreshing scoreboard view.
     */
    private void refreshView()
    {
        if(scoreboard != null)
        {
            TextView textView = (TextView)findViewById(R.id.textView2);

            String scoreboardContent = "NICKNAME \t SCORE\n";

            for(int i = 0 ; i < scoreboard.getScoreboardList().size(); i++)
            {
                ScoreboardElement elem = scoreboard.getScoreboardList().get(i);

                String nickname = elem.getNickname();
                int score = elem.getScore();

                scoreboardContent += nickname + " \t " + score + "\n";
            }

            textView.setText(scoreboardContent);
        }
    }
}
