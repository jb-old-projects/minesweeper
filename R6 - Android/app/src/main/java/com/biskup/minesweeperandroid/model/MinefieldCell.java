package com.biskup.minesweeperandroid.model;


import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * @author Jakub Biskup
 * @version 6.0
 */

public class MinefieldCell
{
    /**
     * Pointer to minefield model variable
     */
    private Minefield minefield;

    /**
     * Cell X-Axis position
     */
    private int posX;

    /**
     * Cell Y-Axis position
     */
    private int posY;

    /**
     * Determines if cell contains bomb
     */
    private boolean containsBomb;

    /**
     * Cell neighbors cell with bomb count
     */
    private int bombsNear;

    /**
     * Determines if cell is marked
     */
    private boolean marked = false;

    /**
     * Determines if cell was opened
     */
    private boolean wasOpened = false;

    /**
     * Cell width
     */
    private int width = 40;

    /**
     * Cell height
     */
    private int height = 40;

    /**
     * Determines if cell text is visible.
     */
    private boolean isTextVisible = false;

    /**
     * Contains cell text string.
     */
    private String textString = " ";

    /**
     * Determines cell fill color.
     */
    private Color cellFillColor;

    /**
     * Determines cell text color.
     */
    private Color textColor;

    public void setCellFillColor(Color cellFillColor) {
        this.cellFillColor = cellFillColor;
    }

    /**
     * Text visibility variable getter.
     * @return true if cell text is visible.
     */
    public boolean isIsTextVisible() {
        return isTextVisible;
    }

    /**
     * Bombs near variable setter.
     * @param bombsNear sets how many bombs are near to current cell.
     */
    void setBombsNear(int bombsNear)
    {
        this.bombsNear = bombsNear;
    }

    /**
     * Constains bomb getter.
     * @return true if current cell contains bomb.
     */
    public boolean containsBomb()
    {
        return containsBomb;
    }

    /**
     * Cell X position getter.
     * @return cell X position.
     */
    int getPosX()
    {
        return posX;
    }

    /**
     * Cell Y position getter.
     * @return cell Y position.
     */
    int getPosY()
    {
        return posY;
    }

    /**
     * Contains bomb setter.
     * @param containsBomb sets if current cell contains bomb.
     */
    void setContainsBomb(boolean containsBomb)
    {
        this.containsBomb = containsBomb;
    }

    /**
     * Cell text getter.
     * @return cell text.
     */
    public String getTextString() {
        if(textString.equals(""))
        {
            return " ";
        }
        else
        {
            return textString;
        }
    }

    /**
     * Cell opened boolean getter.
     * @return true if current cell was opened.
     */
    public boolean isOpened()
    {
        return wasOpened;
    }

    /**
     * Cell marked boolean getter.
     * @return true if current cell is marked.
     */
    public boolean isMarked()
    {
        return marked;
    }

    /**
     * Sets current cell text.
     * @param textString new cell text.
     */
    public void setTextString(String textString)
    {
        this.textString = textString;
    }

    /**
     * MinefieldCell contructor which is setting up Minefield Cell values
     * @param minefield Pointer to minefield object (it is necessary to have access to Game interafces)
     * @param posX X Axis position of cell
     * @param posY Y Axis position of cell
     * @param containsBomb Determines if cell contains bomb
     */

    @RequiresApi(api = Build.VERSION_CODES.O)
    MinefieldCell(Minefield minefield, int posX, int posY, boolean containsBomb)
    {
        this.minefield = minefield;

        this.posX = posX;
        this.posY = posY;
        this.containsBomb = containsBomb;

        isTextVisible = false;
    }

    /**
     * Method which is called after click on the cell
     * @param button button string.
     */

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onMouseClickAction(String button)
    {
        if(button == null || button.equals(""))
        {
            return;
        }

        if(minefield.getGameValues().getGameState() == Game.GameState.GameOver)
        {
            minefield.getGameFlow().restartGame();
            return;
        }

        if(!minefield.getGameValues().getFirstCellOpened())
        {
            if(button.equals("SECONDARY"))
            {
                return;
            }
            else if(button.equals("PRIMARY"))
            {
                minefield.createMinefieldContent(this);
                minefield.getGameValues().setFirstCellOpened(true);
            }
        }

        if(!wasOpened)
        {
            if(button.equals("PRIMARY") && !marked)
            {
                try
                {
                    isTextVisible = true;

                    if (containsBomb)
                    {
                        UnlockBombCell();
                    }
                    else
                    {
                        UnlockNormalCell(button);
                    }
                }
                catch (GameOverException ex)
                {
                    ex.show();
                }
            }
            else if(button.equals("SECONDARY"))
            {
                setCellMarked(!isMarked());
            }
        }
    }

    /**
     * Method which is changing cell state
     * @param marked Variable which is describing if variable is marked or not
     */

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setCellMarked(boolean marked)
    {
        //System.out.println(marked);
        this.marked = marked;

        isTextVisible = marked;

        if(marked)
        {
            textString = " 🚩";
        }
        else
        {
            if(bombsNear > 0)
            {
                textString = String.valueOf(bombsNear);
            }
            else
            {
                textString = "";
            }
        }
    }

    /**
     * Method which is handling normal cell unlock event
     */

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void UnlockNormalCell(String button)
    {
        minefield.getGameValues().setScore(minefield.getGameValues().getScore() + 1);
        wasOpened = true;
        isTextVisible = true;
        cellFillColor = null;

        if(bombsNear > 0)
        {
            textString = String.valueOf(bombsNear);
        }
        else
        {
            textString = "";
        }

        if(textString.isEmpty())
        {
            minefield.getCellNeighbors(this).forEach(neighbor -> neighbor.onMouseClickAction((button)));
        }
    }

    /**
     * Method which is handling bomb cell unlock event
     * @throws GameOverException Exception which is thrown after unlocking bomb cell
     */

    private void UnlockBombCell() throws GameOverException
    {
        wasOpened = true;
        minefield.getGameValues().setGameState(Game.GameState.GameOver);
        throw new GameOverException(this);
    }
}

