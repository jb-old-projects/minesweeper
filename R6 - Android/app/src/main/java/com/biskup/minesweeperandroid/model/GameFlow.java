package com.biskup.minesweeperandroid.model;

/**
 * Game flow interface which is necessary to separate Game class methods
 * @author Jakub Biskup
 * @version 6.0
 */

public interface GameFlow
{
    /**
     * Method which is handling game restart
     */
    void restartGame();
}
