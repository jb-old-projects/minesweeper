package com.biskup.minesweeperandroid;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.RadioGroup;

import com.biskup.minesweeperandroid.model.Game;
import com.biskup.minesweeperandroid.model.Minefield;
import com.biskup.minesweeperandroid.model.MinefieldCell;

/**
 * Class which is managing game view of application.
 * @author Jakub Biskup
 * @version 6.0
 */

public class GameActivity extends AppCompatActivity
{
    /**
     * Pointer to game instance.
     */
    private Game game;

    /**
     * Pointer to minefield grid layout.
     */
    private GridLayout minefieldGridLayout;

    /**
     * Method which is called after user click on minefield cell.
     * @param view reference to clicked object view.
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onMinefieldCellClick(View view)
    {
        String fullResourceName = view.getResources().getResourceName(view.getId());
        String[] splitedName = fullResourceName.split("/");
        String shortName = splitedName[1];
        String indexString = shortName.substring((shortName.indexOf('c') - 1), shortName.length());
        int posX = Integer.parseInt(indexString.split("c")[0]);
        int posY = Integer.parseInt(indexString.split("c")[1]);

        String mouseButton = "";

        if(game.getActionType() == Game.ActionType.OpenCell)
        {
            mouseButton = "PRIMARY";
        }
        else
        {
            mouseButton = "SECONDARY";
        }

        String x = "PosX: " + posX + " PosY: " + posY + " mouseButton:" + mouseButton;

        if(game.getGameState() == Game.GameState.GameOver)
        {
            onRestargGame();
        }

        game.getMinefield().getMinefield().get(posX).get(posY).onMouseClickAction(mouseButton);

        refreshView();
    }

    /**
     * Method which is called after user click on restart button.
     * @param view reference to clicked object view.
     */
    public void onRestartButtonClick(View view)
    {
        game.restartGame();
        onRestargGame();
        refreshView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        RadioGroup actionTypeRadioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
        actionTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.openCellButton)
                {
                    game.setActionType(Game.ActionType.OpenCell);
                }
                else if (i == R.id.markFlagButton)
                {
                    game.setActionType(Game.ActionType.MarkFlag);
                }
            }
        });

        minefieldGridLayout = (GridLayout)findViewById(R.id.minefieldTable);

        addMinefieldCellClickListeners();

        String nickname = getIntent().getStringExtra("userNickname");

        game = new Game(nickname);

        refreshView();
    }


    /**
     * Method which is adding on click listeners to minefield cells.
     */
    private void addMinefieldCellClickListeners() {
        for(int i = 0; i < minefieldGridLayout.getChildCount() ; i++)
        {
            Button btn = (Button) minefieldGridLayout.getChildAt(i);
            btn.setText(String.valueOf(i));
            btn.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View view) {
                    onMinefieldCellClick(view);
                }
            });
        }
    }


    /**
     * Method which is refreshing activity view after user action.
     */
    private void refreshView()
    {
        Minefield minefield = game.getMinefield();

        for(int i = 0; i < minefieldGridLayout.getChildCount() ; i++)
        {
            Button btn = (Button) minefieldGridLayout.getChildAt(i);
            String fullResourceName = getResources().getResourceName(btn.getId());
            String[] splitedName = fullResourceName.split("/");
            String shortName = splitedName[1];
            String indexString = shortName.substring((shortName.indexOf('c') - 1), shortName.length());
            int posX = Integer.parseInt(indexString.split("c")[0]);
            int posY = Integer.parseInt(indexString.split("c")[1]);
            MinefieldCell cell = minefield.getMinefield().get(posX).get(posY);

            if(cell.isOpened())
            {
                btn.setTextColor(Color.BLACK);

                if(!cell.containsBomb())
                {
                    btn.setBackgroundColor(Color.GRAY);
                }
                else
                {
                    btn.setBackgroundColor(Color.RED);
                }
            }

            btn.setText(cell.getTextString());

            if(!cell.isOpened() && !cell.isMarked())
            {
                btn.setText("");
            }
        }
    }

    /**
     * Method which is restarting view to default.
     */
    private void onRestargGame()
    {
        for(int i = 0; i < minefieldGridLayout.getChildCount() ; i++)
        {
            Button btn = (Button) minefieldGridLayout.getChildAt(i);
            btn.setTextColor(Color.WHITE);
            btn.setBackgroundColor(Color.BLACK);
        }
    }
}
