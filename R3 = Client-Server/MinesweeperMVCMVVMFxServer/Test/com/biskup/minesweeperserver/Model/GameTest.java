package com.biskup.minesweeperserver.Model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GameTest
{
    @Test
    void gameRestartingTest()
    {
        // given
        Game game = new Game();
        Minefield minefieldObj = new Minefield(game, game, game);
        game.minefield = minefieldObj;

        game.setFirstCellOpened(true);
        game.setGameState(Game.GameState.GameOver);

        // when
        game.restartGame();

        // then
        Assertions.assertEquals(false, game.getFirstCellOpened());
        Assertions.assertEquals(Game.GameState.Game, game.getGameState());
    }

    @Test
    void onCellClickActionTest()
    {
        // given
        Game game = new Game();
        Minefield minefieldObj = new Minefield(game, game, game);
        game.minefield = minefieldObj;

        // when
        game.onCellClickAction("ONCLICK,0,0,PRIMARY");
        game.onCellClickAction("ONCLICK,2,2,ASDASD");
        game.onCellClickAction("ASDASD,3,3,PRIMARY");
        game.onCellClickAction("");
        game.onCellClickAction("1");
        game.onCellClickAction("1,1");
        game.onCellClickAction("1,4,4");
        game.onCellClickAction("1,5,5,1");
        game.onCellClickAction("1,6,6,1,1");

        // then
        Assertions.assertEquals(true, game.minefield.getMinefield().get(0).get(0).isOpened());
        Assertions.assertEquals(false, game.minefield.getMinefield().get(2).get(2).isOpened());
        Assertions.assertEquals(false, game.minefield.getMinefield().get(3).get(3).isOpened());
        Assertions.assertEquals(false, game.minefield.getMinefield().get(4).get(5).isOpened());
        Assertions.assertEquals(false, game.minefield.getMinefield().get(5).get(5).isOpened());
        Assertions.assertEquals(false, game.minefield.getMinefield().get(6).get(6).isOpened());

    }
}
