package com.biskup.minesweeperserver.Model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MinefieldDataTest
{
    @Test
    public void addCellDataTest()
    {
        // given
        MinefieldData minefieldData1 = new MinefieldData();
        MinefieldData minefieldData2 = new MinefieldData();
        MinefieldData minefieldData3 = new MinefieldData();
        MinefieldCellData minefieldCellData1 = new MinefieldCellData();
        MinefieldCellData minefieldCellData2 = new MinefieldCellData();
        MinefieldCellData minefieldCellData3 = new MinefieldCellData();

        minefieldCellData1.addKeyValuePair("xx", "dd");
        minefieldCellData2.addKeyValuePair("lol", "xd");
        minefieldCellData3.addKeyValuePair("rofl", "haha");

        // when
        minefieldData1.addCellData(minefieldCellData1);
        minefieldData2.addCellData(minefieldCellData1);
        minefieldData2.addCellData(minefieldCellData2);
        minefieldData2.addCellData(minefieldCellData3);
        minefieldData3.addCellData(null);

        String[] minefieldDataInfo1 = minefieldData1.getMinefieldDataString().split(";");
        String[] minefieldDataInfo2 = minefieldData2.getMinefieldDataString().split(";");

        // then
        Assertions.assertEquals(1, minefieldDataInfo1.length);
        Assertions.assertEquals(3, minefieldDataInfo2.length);
        Assertions.assertEquals(0, minefieldData3.getMinefieldDataString().length());
    }
}
