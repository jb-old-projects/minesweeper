package com.biskup.minesweeperserver.Model;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class MinefieldCellDataTest
{
    @Test
    public void addKeyValuePairTest()
    {
        // given
        MinefieldCellData cellData1 = new MinefieldCellData();
        MinefieldCellData cellData2 = new MinefieldCellData();
        MinefieldCellData cellData3 = new MinefieldCellData();
        MinefieldCellData cellData4 = new MinefieldCellData();
        MinefieldCellData cellData5 = new MinefieldCellData();
        MinefieldCellData cellData6 = new MinefieldCellData();
        MinefieldCellData cellData7 = new MinefieldCellData();
        MinefieldCellData cellData8 = new MinefieldCellData();

        // when
        cellData1.addKeyValuePair("x", "d");
        String[] cellDataString1 = cellData1.getMinefieldCellDataString().split(",");
        String[] cellDataStringElem1 = cellDataString1[0].split(":");
        cellData2.addKeyValuePair("", "d");
        String[] cellDataString2 = cellData2.getMinefieldCellDataString().split(",");
        String[] cellDataStringElem2 = cellDataString2[0].split(":");
        cellData3.addKeyValuePair("x", "");
        String[] cellDataString3 = cellData3.getMinefieldCellDataString().split(",");
        cellData4.addKeyValuePair("", "");
        String[] cellDataString4 = cellData4.getMinefieldCellDataString().split(",");
        cellData5.addKeyValuePair(null, "");
        String[] cellDataString5 = cellData5.getMinefieldCellDataString().split(",");
        cellData6.addKeyValuePair(null, null);
        String[] cellDataString6 = cellData6.getMinefieldCellDataString().split(",");
        cellData7.addKeyValuePair("x", "d");
        cellData7.addKeyValuePair("yyy", "zzzz");
        String[] cellDataString7 = cellData7.getMinefieldCellDataString().split(",");
        cellData8.addKeyValuePair("xyz", "ddddd");
        cellData8.addKeyValuePair(null, null);
        cellData8.addKeyValuePair("asdasd", "lol");
        String[] cellDataString8 = cellData8.getMinefieldCellDataString().split(",");

        // then
        Assertions.assertEquals(1, cellDataString1.length);
        Assertions.assertEquals(2, cellDataStringElem1.length);
        Assertions.assertEquals(1, cellDataString2.length);
        Assertions.assertEquals(2, cellDataStringElem2.length);
        Assertions.assertEquals(1, cellDataString3.length);
        Assertions.assertEquals(1, cellDataString4.length);
        Assertions.assertEquals(1, cellDataString5.length);
        Assertions.assertEquals(1, cellDataString6.length);
        Assertions.assertEquals(2, cellDataString7.length);
        Assertions.assertEquals(3, cellDataString8.length);
    }
}
