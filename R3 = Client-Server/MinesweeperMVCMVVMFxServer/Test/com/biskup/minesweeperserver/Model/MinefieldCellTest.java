package com.biskup.minesweeperserver.Model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MinefieldCellTest
{
    @Test
    void onMouseClickActionTest()
    {
        // given
        Game game = new Game();
        Minefield minefieldObj = new Minefield(game, game, game);
        game.minefield = minefieldObj;

        // when
        MinefieldCell cell1 = game.minefield.getMinefield().get(0).get(0);
        cell1.onMouseClickAction("PRIMARY");
        MinefieldCell cell2 = game.minefield.getMinefield().get(1).get(1);
        cell2.onMouseClickAction("SECONDARY");
        MinefieldCell cell3 = game.minefield.getMinefield().get(2).get(2);
        cell3.onMouseClickAction("SECONDARY");
        cell3.onMouseClickAction("SECONDARY");
        MinefieldCell cell4 = game.minefield.getMinefield().get(3).get(3);
        MinefieldCell cell4tmp = game.minefield.getMinefield().get(3).get(3);
        cell4.onMouseClickAction("ASDASD");
        MinefieldCell cell5 = game.minefield.getMinefield().get(4).get(4);
        MinefieldCell cell5tmp = game.minefield.getMinefield().get(4).get(4);
        cell5.onMouseClickAction("");
        MinefieldCell cell6 = game.minefield.getMinefield().get(5).get(5);
        MinefieldCell cell6tmp = game.minefield.getMinefield().get(5).get(5);
        cell6.onMouseClickAction(null);
        MinefieldCell cell7 = game.minefield.getMinefield().get(6).get(6);
        cell7.onMouseClickAction("PRIMARY");
        cell7.onMouseClickAction("SECONDARY");
        MinefieldCell cell8 = game.minefield.getMinefield().get(7).get(7);
        cell8.onMouseClickAction("SECONDARY");
        cell8.onMouseClickAction("PRIMARY");
        MinefieldCell cell9 = game.minefield.getMinefield().get(8).get(8);
        cell9.onMouseClickAction("SECONDARY");
        cell9.onMouseClickAction("PRIMARY");
        cell9.onMouseClickAction("SECONDARY");
        cell9.onMouseClickAction("PRIMARY");

        // then
        Assertions.assertTrue(cell1.isOpened());

        if(cell2.isOpened())
        {
            Assertions.assertTrue(!cell2.isMarked());
        }
        else
        {
            Assertions.assertTrue(cell2.isMarked());
        }

        Assertions.assertTrue(!cell3.isMarked());
        Assertions.assertEquals(cell4tmp, cell4);
        Assertions.assertEquals(cell5tmp, cell5);
        Assertions.assertEquals(cell6tmp, cell6);
        Assertions.assertTrue(!cell7.isMarked());

        if(cell8.isOpened())
        {
            Assertions.assertTrue(!cell8.isMarked());
        }
        else
        {
            Assertions.assertTrue(cell8.isMarked());
        }

        Assertions.assertTrue(cell9.isOpened());
    }
}
