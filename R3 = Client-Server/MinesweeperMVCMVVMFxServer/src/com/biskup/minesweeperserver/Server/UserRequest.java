package com.biskup.minesweeperserver.Server;

/**
 * @author Jakub Biskup
 * @version 3.0
 * Enumeration type containing user requests from Client.
 */

public enum UserRequest
{
    Quit,
}
