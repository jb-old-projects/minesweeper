package com.biskup.minesweeperserver.Server;

/**
 * @author Jakub Biskup
 * @version 3.0
 * User disconected from server exception class.
 */

public class UserDisconnectedFromServerException extends Exception
{
    /**
     * Exception contructor
     * @param s exception message
     */
    public UserDisconnectedFromServerException(String s)
    {
        super(s);
    }
}
