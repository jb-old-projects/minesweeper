package com.biskup.minesweeperserver.Server;

import com.biskup.minesweeperserver.Model.Game;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

/**
 * @author Jakub Biskup
 * @version 3.0
 * Server class.
 */

public class Server
{
    private Properties properties = new Properties();
    private ServerSocket serverSocket;

    /**
     * Constructor of Server class.
     */
    public Server()
    {
        try
        {
            FileInputStream inputStream = new FileInputStream("D:\\Programowanie\\Java\\IdeaProjects\\MinesweeperMVCMVVMFxServer\\src\\com\\biskup\\minesweeperserver\\Server\\server.properties");
            properties.load(inputStream);
            serverSocket = new ServerSocket(Integer.parseInt(properties.getProperty("port")));
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("En error has occured when loading properties!\n");
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            System.out.println("An error has occured when loading properties!\n");
            ex.printStackTrace();
        }
    }

    /**
     * Main method of Server class.
     * @param args the command line arguments.
     */
    public static void main(String[] args)
    {
        initServer();
    }

    /**
     * Method which is initializing server.
     */
    private static void initServer()
    {
        Socket socket;
        Server serverTCP = null;
        Game game = new Game();

        try
        {
            serverTCP = new Server();
            System.out.println("Server started!");

            while(true)
            {
                socket = serverTCP.serverSocket.accept();

                try
                {
                    Service service = new Service(game, socket);
                    service.realize();
                }
                catch (IOException ex)
                {
                    socket.close();
                    System.err.println(ex.getMessage());
                    break;
                }
                catch (UserDisconnectedFromServerException ex)
                {
                    System.out.println(ex.getMessage());
                }
            }
        }
        catch (IOException ex)
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            if(serverTCP.serverSocket != null)
            {
                try
                {
                    serverTCP.serverSocket.close();
                }
                catch (IOException ex)
                {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }
}
