package com.biskup.minesweeperserver.Server;

import com.biskup.minesweeperserver.Model.Game;

import java.io.*;
import java.net.Socket;

/**
 * @author Jakub Biskup
 * @version 3.0
 * Single service class.
 */

public class Service
{
    private Socket socket;
    private BufferedReader inputBufferedReader;
    private PrintWriter outputPrintWriter;
    private Game game;
    private boolean started = false;

    /**
     * Constructor of Service class object.
     * @param game game class instance
     * @param socket socket used for initialization
     * @throws IOException throws exception if there is a problem with connection
     */
    public Service(Game game, Socket socket) throws IOException
    {
        this.socket = socket;
        this.game = game;
        outputPrintWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        inputBufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    /**
     * Sends the response to Client.
     * @param response ServerAdditionalResponse enum object
     */
    public void sendResponse(ServerAdditionalResponse response)
    {
        outputPrintWriter.println(response.name());
    }

    /**
     * Sends the response to Client.
     * @param response string response
     */
    public void sendResponse(String response)
    {
        outputPrintWriter.println(response);
    }

    /**
     * Sends the response to Client.
     * @param response int response
     */
    public void sendResponse(int response)
    {
        outputPrintWriter.println(String.valueOf(response));
    }

    /**
     * Realization of client commands.
     */
    public void realize() throws UserDisconnectedFromServerException
    {
        try
        {
            while(true)
            {
                String userRequest = "";

                try
                {
                    userRequest = inputBufferedReader.readLine();
                    userRequest = userRequest.toUpperCase();
                }
                catch (IOException ex)
                {
                    throw new UserDisconnectedFromServerException("User disconnected!");
                }
                catch (NullPointerException ex)
                {
                    outputPrintWriter.println("An error has occured while reading message from user!\n");
                    ex.printStackTrace();
                }

                if(!started)
                {
                    game = new Game();
                    System.out.println("Game started!");
                    started = true;
                }

                if(!userRequest.isEmpty())
                {
                    System.out.println("User Request: " + userRequest);
                }

                if(userRequest.equals(UserRequest.Quit.name().toUpperCase()))
                {
                    sendResponse(ServerAdditionalResponse.Disconnected);
                    break;
                }

                if(userRequest.equals("HELP"))
                {
                    sendResponse("AVAILABLE COMMANDS");
                    sendResponse("");
                    sendResponse("WITHOUT PARAMETERS:");
                    sendResponse("Get,Cell_Width");
                    sendResponse("Get,Cell_Height");
                    sendResponse("Get,Cell_Count_X");
                    sendResponse("Get,Cell_Count_Y");
                    sendResponse("Quit");
                    sendResponse("");
                    sendResponse("WITH PARAMETERS:");
                    sendResponse("OnClick,A,B,C");
                    sendResponse("");
                    sendResponse("WHERE");
                    sendResponse("A is cell X-Axis position");
                    sendResponse("B is cell Y-Axis position");
                    sendResponse("C is cell used mouse button (PRIMARY or SECONDARY)");
                }

                String[] userRequests = userRequest.split(",");

                switch(userRequests[0])
                {
                    case "GET":
                        switch(userRequests[1])
                        {
                            case "CELL_WIDTH":
                                sendResponse(40);
                                break;
                            case "CELL_HEIGHT":
                                sendResponse(40);
                                break;
                            case "CELL_COUNT_X":
                                sendResponse(20);
                                break;
                            case "CELL_COUNT_Y":
                                sendResponse(20);
                                break;
                        }
                        break;
                    case "ONCLICK":
                        game.onCellClickAction(userRequest);
                        sendResponse(game.getMinefieldData());
                        break;

                }
            }
        }
        catch (NullPointerException ex)
        {
            sendResponse(ServerAdditionalResponse.UnexceptedError);
        }
        finally
        {
            try
            {
                socket.close();
            }
            catch (IOException ex)
            {
                System.err.print(ex.getMessage());
            }
        }
    }
}
