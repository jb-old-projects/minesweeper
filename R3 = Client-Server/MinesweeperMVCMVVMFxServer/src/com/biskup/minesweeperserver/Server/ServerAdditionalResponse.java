package com.biskup.minesweeperserver.Server;

/**
 * @author Jakub Biskup
 * @version 3.0
 * Enumeration type containing responses for Client..
 */

public enum ServerAdditionalResponse
{
    Disconnected,
    UnexceptedError
}
