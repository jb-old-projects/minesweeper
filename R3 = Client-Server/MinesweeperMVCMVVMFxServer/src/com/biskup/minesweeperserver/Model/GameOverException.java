package com.biskup.minesweeperserver.Model;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class GameOverException extends Exception
{
    /**
     * Pointer to minefield cell view
     */
    private MinefieldCell minefieldCell;

    /**
     * GameOverException constructor
     * @param minefieldCell Pointer to minefield cell
     */
    public GameOverException(MinefieldCell minefieldCell)
    {
        this.minefieldCell = minefieldCell;
    }

    /**
     * Method which is called after unlocking bomb cell
     */
    public void show()
    {
        minefieldCell.setTextString("X");
        System.out.println("Game Over!");
    }
}
