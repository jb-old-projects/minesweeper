package com.biskup.minesweeperserver.Model;

import java.util.List;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class Game implements GameValues, GameFlow
{
    /**
     * Pointer to minefield
     */
    protected Minefield minefield;

    /**
     * Current game state
     */
    private GameState gameState;

    /**
     * Determines if user clicked first cell
     */
    private boolean firstCellOpened = false;

    @Override
    public boolean getFirstCellOpened() {
            return firstCellOpened;
    }

    @Override
    public GameState getGameState() {
        return gameState;
    }

    @Override
    public void setFirstCellOpened(boolean firstCellOpened)
    {
        this.firstCellOpened = firstCellOpened;
    }

    @Override
    public void setGameState(GameState gameState)
    {
        this.gameState = gameState;
    }

    public enum GameState
    {
        Game,
        GameOver
    }

    /**
     * Game class contructor which is calling gameInit method.
     */

    public Game()
    {
        gameInit();
    }

    /**
     *  Restores default game values and cleares minefield.
     */

    public void restartGame()
    {
        minefield = new Minefield(this,this, this);
        initDefaultValues();
    }

    /**
     * Method which is called when user sent user request with on click action.
     * @param clickInfo user click information as string which contains click position and mouse button.
     */

    public void onCellClickAction(String clickInfo)
    {
        System.out.println(clickInfo);
        List<List<MinefieldCell>> minefieldList =  minefield.getMinefield();
        String clickInfos[] = clickInfo.split(",");

        if(clickInfos.length != 4 || !clickInfo.contains("ONCLICK"))
        {
            System.out.println("Invalid click info!");
            return;
        }

        if(!clickInfos[3].equals("PRIMARY") && !clickInfos[3].equals("SECONDARY"))
        {
            System.out.println("Invalid button click info!");
            return;
        }

        for(List<MinefieldCell> row : minefieldList)
        {
            for(MinefieldCell cell : row)
            {
                if(clickInfos[1].equals(String.valueOf(cell.getPosX())) && clickInfos[2].equals(String.valueOf(cell.getPosY())))
                {
                    cell.onMouseClickAction(clickInfos[3]);
                }
            }
        }
    }

    /**
     * Method which is getting minefield data as string.
     * @return minefield data as string.
     */

    public String getMinefieldData()
    {
        return minefield.getMinefieldDataAsString();
    }

    /**
     * Game initializing method
     */

    private void gameInit()
    {
        minefield = new Minefield(this, this, this);
        initDefaultValues();
    }

    /**
     * Method which is initializing default game values
     */

    private void initDefaultValues()
    {
        gameState = GameState.Game;
        firstCellOpened = false;
    }
}
