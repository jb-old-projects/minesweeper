package com.biskup.minesweeperserver.Model;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class Minefield
{
    /**
     * Two dimensional list of Minefield Cells which is representing minefield
     */
    List<List<MinefieldCell>> minefield;

    /**
     * List of points which can be neighbor to cell
     */
    List<Point> neighborsPoints;

    /**
     * Game values interface
     */
    private GameValues gameValues;

    /**
     * Game flow interface
     */
    private GameFlow gameFlow;

    /**
     * Minefield cells in row count
     */
    private final int cellCountX = 20;

    /**
     * Minefield cells in column count
     */
    private final int cellCountY = 20;

    /**
     * Minefield bomb spawn probability
     */
    private final float bombProbability = 0.2f;

    /**
     * Game values interface getter.
     * @return returns game values interface.
     */
    GameValues getGameValues()
    {
        return gameValues;
    }

    /**
     * Game flow interface getter.
     * @return returns game flow interface.
     */
    GameFlow getGameFlow()
    {
        return gameFlow;
    }

    /**
     * Minefield variable.
     * @return returns minefield.
     */
    List<List<MinefieldCell>> getMinefield()
    {
        return minefield;
    }

    /**
     * Minefield class contructor which is creating empty minefield
     * @param game pointer to an instance of game class
     * @param gameValues Game values interface
     * @param gameFlow Game flow interface
     */

    public Minefield(Game game, GameValues gameValues, GameFlow gameFlow)
    {
        if(game != null)
        {
            this.gameValues = gameValues;
            this.gameFlow = gameFlow;
            createEmptyMinefield();
        }
    }

    /**
     * Method which is creating minefield data as string.
     * @return minefield data as string.
     */

    public String getMinefieldDataAsString()
    {
        MinefieldData minefieldData = new MinefieldData();

        for (List<MinefieldCell> row : minefield)
        {
            for(MinefieldCell cell : row)
            {
                MinefieldCellData cellData = new MinefieldCellData();
                cellData.addKeyValuePair("POS_X", String.valueOf(cell.getPosX()));
                cellData.addKeyValuePair("POS_Y", String.valueOf(cell.getPosY()));
                cellData.addKeyValuePair("TEXT", cell.getTextString());
                cellData.addKeyValuePair("IS_TEXT_VISIBLE", Boolean.toString(cell.isIsTextVisible()));
                cellData.addKeyValuePair("WAS_OPENED", Boolean.toString(cell.isOpened()));
                cellData.addKeyValuePair("IS_MARKED", Boolean.toString(cell.isMarked()));
                cellData.addKeyValuePair("CONTAINS_BOMB", Boolean.toString(cell.containsBomb()));
                minefieldData.addCellData(cellData);
            }
        }

        return minefieldData.getMinefieldDataString();
    }

    /**
     * Method which is creating minefield content (it is deciding which cell has bomb)
     * @param firstCell Cell which was clicked on the beginning by user (and should not contains bomb)
     */

    void createMinefieldContent(MinefieldCell firstCell)
    {
        neighborsPoints = new ArrayList<Point>
                (
                        Arrays.asList
                                (
                                        new Point(-1, -1),
                                        new Point(-1, 0),
                                        new Point(-1, 1),
                                        new Point(0, -1),
                                        new Point(0, 1),
                                        new Point(1, -1),
                                        new Point(1, 0),
                                        new Point(1, 1)
                                )
                );

        for (List<MinefieldCell> row : minefield)
        {
            for(MinefieldCell cell : row)
            {
                if(cell != firstCell)
                {
                    cell.setContainsBomb(Math.random() < bombProbability);
                }
                else
                {
                    cell.setContainsBomb(false);
                }
            }
        }

        for (List<MinefieldCell> row : minefield)
        {
            for(MinefieldCell cell : row)
            {
                if(!cell.containsBomb())
                {
                    int bombsCount  = (int)getCellNeighbors(cell).stream().filter(MinefieldCell::containsBomb).count();

                    if(bombsCount > 0)
                    {
                        cell.setBombsNear(bombsCount);
                    }
                }
            }
        }
    }

    /**
     * Method which is searching for cell neighbors
     * @param cell A cell for which method will be searching for neighbor
     * @return Method is returning list of cell neighbors
     */

    List<MinefieldCell> getCellNeighbors(MinefieldCell cell)
    {
        List<MinefieldCell> neighbors = new ArrayList<>();

        if(cell == null)
        {
            return neighbors;
        }

        for(Point cellPoint : neighborsPoints)
        {
            int dx = cellPoint.x;
            int dy = cellPoint.y;

            int newX = cell.getPosX() + dx;
            int newY = cell.getPosY() + dy;

            if(newX >= 0 && newX < cellCountX && newY >= 0 && newY < cellCountY)
            {
                neighbors.add(minefield.get(newX).get(newY));
            }
        }

        return neighbors;
    }

    /**
     * Method which is creating empty minefield (it initializes list of MinefieldCells with proper values)
     */

    private void createEmptyMinefield()
    {
        minefield = new ArrayList<List<MinefieldCell>>();

        for(int x = 0 ; x < cellCountX ; x++)
        {
            minefield.add(new ArrayList<MinefieldCell>());

            for(int y = 0 ; y < cellCountY ; y++)
            {
                MinefieldCell cell = new MinefieldCell(this, x, y, false);

                minefield.get(x).add(cell);
            }
        }
    }
}
