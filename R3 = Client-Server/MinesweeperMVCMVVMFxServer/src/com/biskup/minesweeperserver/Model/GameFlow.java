package com.biskup.minesweeperserver.Model;

/**
 * Game flow interface which is necessary to separate Game class methods
 * @author Jakub Biskup
 * @version 3.0
 */

public interface GameFlow
{
    /**
     * Method which is handling game restart
     */
    void restartGame();
}
