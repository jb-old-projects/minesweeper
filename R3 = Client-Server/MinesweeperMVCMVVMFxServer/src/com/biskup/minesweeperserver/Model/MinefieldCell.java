package com.biskup.minesweeperserver.Model;

import javafx.beans.property.*;
import javafx.scene.paint.Color;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class MinefieldCell
{
    /**
     * Pointer to minefield model variable
     */
    private Minefield minefield;

    /**
     * Cell X-Axis position
     */
    private int posX;

    /**
     * Cell Y-Axis position
     */
    private int posY;

    /**
     * Determines if cell contains bomb
     */
    private BooleanProperty containsBomb = new SimpleBooleanProperty();

    /**
     * Cell neighbors cell with bomb count
     */
    private IntegerProperty bombsNear = new SimpleIntegerProperty();

    /**
     * Determines if cell is marked
     */
    private BooleanProperty marked = new SimpleBooleanProperty(false);

    /**
     * Determines if cell was opened
     */
    private BooleanProperty wasOpened = new SimpleBooleanProperty(false);

    /**
     * Cell width
     */
    private int width = 40;

    /**
     * Cell height
     */
    private int height = 40;

    /**
     * Determines if cell text is visible.
     */
    private BooleanProperty isTextVisible = new SimpleBooleanProperty(false);

    /**
     * Contains cell text string.
     */
    private StringProperty textString = new SimpleStringProperty(" ");

    /**
     * Determines cell fill color.
     */
    private ObjectProperty<Color> cellFillColor = new SimpleObjectProperty<>();

    /**
     * Determines cell text color.
     */
    private ObjectProperty<Color> textColor = new SimpleObjectProperty<>();

    /**
     * Text visibility variable getter.
     * @return true if cell text is visible.
     */
    public boolean isIsTextVisible() {
        return isTextVisible.getValue();
    }

    /**
     * Bombs near variable setter.
     * @param bombsNear sets how many bombs are near to current cell.
     */
    void setBombsNear(int bombsNear)
    {
        this.bombsNear.setValue(bombsNear);
    }

    /**
     * Constains bomb getter.
     * @return true if current cell contains bomb.
     */
    boolean containsBomb()
    {
        return containsBomb.getValue();
    }

    /**
     * Cell X position getter.
     * @return cell X position.
     */
    int getPosX()
    {
        return posX;
    }

    /**
     * Cell Y position getter.
     * @return cell Y position.
     */
    int getPosY()
    {
        return posY;
    }

    /**
     * Contains bomb setter.
     * @param containsBomb sets if current cell contains bomb.
     */
    void setContainsBomb(boolean containsBomb)
    {
        this.containsBomb.setValue(containsBomb);
    }

    /**
     * Cell text getter.
     * @return cell text.
     */
    public String getTextString() {
        if(textString.getValue().equals(""))
        {
            return " ";
        }
        else
        {
            return textString.getValue();
        }
    }

    /**
     * Cell opened boolean getter.
     * @return true if current cell was opened.
     */
    public boolean isOpened()
    {
        return wasOpened.getValue();
    }

    /**
     * Cell marked boolean getter.
     * @return true if current cell is marked.
     */
    public boolean isMarked()
    {
        return marked.getValue();
    }

    /**
     * Sets current cell text.
     * @param textString new cell text.
     */
    public void setTextString(String textString)
    {
        this.textString.setValue(textString);
    }

    /**
     * MinefieldCell contructor which is setting up Minefield Cell values
     * @param minefield Pointer to minefield object (it is necessary to have access to Game interafces)
     * @param posX X Axis position of cell
     * @param posY Y Axis position of cell
     * @param containsBomb Determines if cell contains bomb
     */

    MinefieldCell(Minefield minefield, int posX, int posY, boolean containsBomb)
    {
        this.minefield = minefield;

        this.posX = posX;
        this.posY = posY;
        this.containsBomb.setValue(containsBomb);

        cellFillColor.setValue(Color.BLACK);
        isTextVisible.setValue(false);
        textColor.setValue(Color.RED);
    }

    /**
     * Method which is called after click on the cell
     * @param button button string.
     */

    public void onMouseClickAction(String button)
    {
        if(button == null || button.equals(""))
        {
            return;
        }

        if(minefield.getGameValues().getGameState() == Game.GameState.GameOver)
        {
            minefield.getGameFlow().restartGame();
            return;
        }

        if(!minefield.getGameValues().getFirstCellOpened())
        {
            if(button.equals("SECONDARY"))
            {
                return;
            }
            else if(button.equals("PRIMARY"))
            {
                minefield.createMinefieldContent(this);
                minefield.getGameValues().setFirstCellOpened(true);
            }
        }

        if(!wasOpened.getValue())
        {
            if(button.equals("PRIMARY") && !marked.getValue())
            {
                try
                {
                    isTextVisible.setValue(true);

                    if (containsBomb.getValue())
                    {
                        UnlockBombCell();
                    }
                    else
                    {
                        UnlockNormalCell(button);
                    }
                }
                catch (GameOverException ex)
                {
                    ex.show();
                }
            }
            else if(button.equals("SECONDARY"))
            {
                setCellMarked(!isMarked());
            }
        }
    }

    /**
     * Method which is changing cell state
     * @param marked Variable which is describing if variable is marked or not
     */

    private void setCellMarked(boolean marked)
    {
        //System.out.println(marked);
        this.marked.setValue(marked);

        isTextVisible.setValue(marked);

        if(marked)
        {
            textColor.setValue(Color.RED);
            textString.setValue("!");
        }
        else
        {
            textColor.setValue(Color.BLACK);

            if(bombsNear.getValue() > 0)
            {
                textString.setValue(String.valueOf(bombsNear.getValue()));
            }
            else
            {
                textString.setValue("");
            }
        }
    }

    /**
     * Method which is handling normal cell unlock event
     */

    private void UnlockNormalCell(String button)
    {
        wasOpened.setValue(true);
        textColor.setValue(Color.BLACK);
        isTextVisible.setValue(true);
        cellFillColor.setValue(null);

        if(bombsNear.getValue() > 0)
        {
            textString.setValue(String.valueOf(bombsNear.getValue()));
        }
        else
        {
            textString.setValue("");
        }

        if(textString.getValue().isEmpty())
        {
            minefield.getCellNeighbors(this).forEach(neighbor -> neighbor.onMouseClickAction((button)));
        }
    }

    /**
     * Method which is handling bomb cell unlock event
     * @throws GameOverException Exception which is thrown after unlocking bomb cell
     */

    private void UnlockBombCell() throws GameOverException
    {
        wasOpened.setValue(true);
        minefield.getGameValues().setGameState(Game.GameState.GameOver);
        throw new GameOverException(this);
    }
}
