package com.biskup.minesweeperserver.Model;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class MinefieldData
{
    /**
     * Minefield data string variable.
     */
    private String minefieldDataString = "";

    /**
     * Method which is adding cell data to minefield data.
     * @param cellData cell data to add.
     */
    public void addCellData(MinefieldCellData cellData)
    {
        if(cellData == null)
        {
            return;
        }

        if(!minefieldDataString.equals(""))
        {
            minefieldDataString += ";";
        }

        minefieldDataString += cellData.getMinefieldCellDataString();
    }

    /**
     * Minefield data string getter.
     * @return minefield data string.
     */
    public String getMinefieldDataString()
    {
        return minefieldDataString;
    }
}
