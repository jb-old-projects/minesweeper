package com.biskup.minesweeperserver.Model;

/**
 * Game values interface which is necessary to separate Game class methods. Contains getters and setters.
 * @author Jakub Biskup
 * @version 3.0
 */

public interface GameValues
{
    boolean getFirstCellOpened();
    Game.GameState getGameState();
    void setFirstCellOpened(boolean firstCellOpened);
    void setGameState(Game.GameState gameState);
}
