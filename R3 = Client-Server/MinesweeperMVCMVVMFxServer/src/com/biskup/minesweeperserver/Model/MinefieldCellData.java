package com.biskup.minesweeperserver.Model;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class MinefieldCellData
{
    /**
     * Minefield cell data as string.
     */
    private String minefieldCellDataString = "";

    /**
     * Method which is adding key-value pair to minefield cell data string.
     * @param key pair key string.
     * @param value pair value string.
     */
    public void addKeyValuePair(String key, String value)
    {
        if(!minefieldCellDataString.equals(""))
        {
            minefieldCellDataString += ",";
        }
        String pairToAdd = key + ":" + value;
        minefieldCellDataString += pairToAdd;
    }

    /**
     * Minefield cell data string getter.
     * @return minefield cell data string.
     */
    public String getMinefieldCellDataString()
    {
        return minefieldCellDataString;
    }
}
