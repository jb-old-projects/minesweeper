package com.biskup.minesweeperclient.Controller;

/**
 * @author Jakub Biskup
 * @version 3.0
 * Server communication interface which is necessary to separate GameClient class methods
 */

public interface ServerCommunication
{
    String sendCommandToServer(String command);
}
