package com.biskup.minesweeperclient.Controller;

import com.biskup.minesweeperclient.Views.GameView;
import com.biskup.minesweeperclient.Views.Minefield;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.*;
import java.net.Socket;
import java.util.Properties;

/**
 * @author Jakub Biskup
 * @version 3.0
 * Client class.
 */

public class GameClient extends Application implements ServerCommunication
{
    /**
     * Pointer to game view instance.
     */
    private GameView gameView;

    /**
     * Pointer to minefield instance.
     */
    private Minefield minefield;

    /**
     * Client socket variable.
     */
    private Socket clientSocket;

    /**
     * Data output stream variable.
     */
    private DataOutputStream dataOutputStream;

    /**
     * Buffered reader from server variable.
     */
    private BufferedReader bufferedReaderFromSerer;

    /**
     * Main method of JavaFX Application
     * @param stage stage of game window
     * @throws Exception JavaFX run exception
     */

    @Override
    public void start(Stage stage) throws Exception
    {
        gameClientInit(stage);
    }

    /**
     * This method sends command to the server.
     * @param command command which will be send
     * @return response from the server
     */
    public String sendCommandToServer(String command)
    {
        String response = "";
        command += "\n";
        try
        {
            dataOutputStream.writeBytes(command);

            response = bufferedReaderFromSerer.readLine();
        }
        catch (IOException ex)
        {
            System.out.println("An error has occured while sending command to server!\n" + ex.getMessage());
        }

        return response;
    }

    /**
     * Method which is initializing game client.
     * @param stage stage of game window.
     */
    private void gameClientInit(Stage stage)
    {
        connectToServer();
        createMinefield(stage);
    }

    /**
     * Method which is creating minfield.
     * @param stage stage of game window.
     */
    private void createMinefield(Stage stage)
    {
        int cellWidth = Integer.parseInt(sendCommandToServer("GET,Cell_Width"));
        int cellHeight = Integer.parseInt(sendCommandToServer("GET,Cell_Height"));
        int cellCountX = Integer.parseInt(sendCommandToServer("GET,Cell_Count_X"));
        int cellCountY = Integer.parseInt(sendCommandToServer("GET,Cell_Count_Y"));
        minefield = new Minefield(this, cellCountX, cellCountY, cellWidth, cellHeight);
        gameView = new GameView(stage, minefield.getMinefield());
    }

    /**
     * Method which is making connection with the server
     */
    private void connectToServer()
    {
        Properties properties = new Properties();
        try
        {
            FileInputStream fileInputStream = new FileInputStream("D:\\Programowanie\\Java\\IdeaProjects\\MinesweeperMVCMVVMFxClient\\src\\com\\biskup\\minesweeperclient\\Controller\\client.properties");
            properties.load(fileInputStream);
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("An error has occured while loading properties!\n");
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            System.out.println("An error has occurred while loading properties!\n");
            ex.printStackTrace();
        }

        try
        {
            int port = Integer.parseInt(properties.getProperty("port"));
            String address = properties.getProperty("address");
            clientSocket = new Socket(address, port);
            dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
            bufferedReaderFromSerer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        }
        catch (IOException ex)
        {
            System.out.println("An error has occurred while connecting to the server!\n");
            ex.printStackTrace();
        }
    }
}
