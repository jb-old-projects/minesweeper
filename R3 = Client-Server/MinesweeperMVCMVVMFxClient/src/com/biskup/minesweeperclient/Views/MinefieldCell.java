package com.biskup.minesweeperclient.Views;

import com.biskup.minesweeperclient.Controller.ServerCommunication;
import javafx.beans.property.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class MinefieldCell extends StackPane
{
    /**
     * Pointer to minefield class instance.
     */
    private Minefield minefield;

    /**
     * Pointer to minefield cell view class instance.
     */
    private MinefieldCellView minefieldCellView;

    /**
     * Pointer to game client server communication interface.
     */
    private ServerCommunication serverCommunication;

    /**
     * Cell X-Axis position.
     */
    private int posX;

    /**
     * Cell Y-Axis position.
     */
    private int posY;

    /**
     * Cell width.
     */
    private int width;

    /**
     * Cell heiht.
     */
    private int height;

    /**
     * Boolean which is describing if cell is marked.
     */
    private boolean marked;

    /**
     * Boolean which is describing if cell was opened.
     */
    private boolean opened;

    /**
     * Boolean which is describing if cell constains bomb.
     */
    private boolean containsBomb;

    /**
     * Boolean property which is describing if cell text is visible.
     */
    private BooleanProperty isTextVisible = new SimpleBooleanProperty(false);

    /**
     * Cell text variable.
     */
    private StringProperty textString = new SimpleStringProperty("");

    /**
     * Text visible getter.
     * @return returns true if cell text is visible.
     */
    public boolean isTextVisible() {
        return isTextVisible.getValue();
    }

    /**
     * Cell text variable getter.
     * @return returs cell text as string.
     */
    public String getTextString() {
        return textString.getValue();
    }

    /**
     * Contains bomb getter.
     * @return returns true if cell contains bomb.
     */
    public boolean hasBomb()
    {
        return containsBomb;
    }

    /**
     * Marked variable getter.
     * @return returns true if cell is marked.
     */
    public boolean isMarked()
    {
        return marked;
    }

    /**
     * Opened variable getter.
     * @return returns true if cell was opened.
     */
    public boolean isOpened()
    {
        return opened;
    }

    /**
     * MinefieldCell class constructor.
     * @param serverCommunication pointer to server communication interface.
     * @param minefield pointer to minefield.
     * @param x cell X-Axis position.
     * @param y cell Y-Axis position.
     * @param width cell width.
     * @param height cell height.
     */
    public MinefieldCell(ServerCommunication serverCommunication, Minefield minefield, int x, int y, int width, int height)
    {
        this.serverCommunication = serverCommunication;
        this.minefield = minefield;

        this.posX = x;
        this.posY = y;
        this.width = width;
        this.height = height;

        minefieldCellView = new MinefieldCellView(getChildren(), this.width, this.height);

        setTranslateX(posX * width);
        setTranslateY(posY * height);

        setOnMouseClicked(this::onMouseClickAction);
    }

    /**
     * Method which is refreshing cell view.
     * @param cellInformationString cell information string which is necessary to refresh cell view.
     */
    public void refreshView(String cellInformationString)
    {
        String cellInfos[] = cellInformationString.split(",");

        for(String info : cellInfos)
        {
            String keyValuePair[] = info.split(":");
            switch(keyValuePair[0])
            {
                case "TEXT":
                    this.textString.setValue(keyValuePair[1]);
                    break;
                case "IS_TEXT_VISIBLE":
                    this.isTextVisible.setValue(Boolean.parseBoolean(keyValuePair[1]));
                    break;
                case "IS_MARKED":
                    this.marked = Boolean.parseBoolean(keyValuePair[1]);
                    break;
                case "WAS_OPENED":
                    this.opened = Boolean.parseBoolean(keyValuePair[1]);
                    break;
                case "CONTAINS_BOMB":
                    this.containsBomb = Boolean.parseBoolean(keyValuePair[1]);
                    break;
            }
        }

        minefieldCellView.refresh(this);
    }

    /**
     * Method which is called on user mouse click.
     * @param mouseEvent mouse event describing event which occurred.
     */
    private void onMouseClickAction(MouseEvent mouseEvent)
    {
        String serverRequest = "OnClick,";
        serverRequest += String.valueOf(posX) + ",";
        serverRequest += String.valueOf(posY) + ",";
        serverRequest += String.valueOf(mouseEvent.getButton());
        String serverResponse = serverCommunication.sendCommandToServer(serverRequest);
        minefield.refreshView(serverResponse);
    }
}
