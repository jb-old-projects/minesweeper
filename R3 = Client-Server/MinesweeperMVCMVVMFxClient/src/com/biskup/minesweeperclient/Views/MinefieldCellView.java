package com.biskup.minesweeperclient.Views;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class MinefieldCellView
{
    /**
     * Pointer to cell rectangle.
     */
    private Rectangle fieldRect;

    /**
     * Cell text variable.
     */
    private Text text;

    /**
     * MinefieldCellView class constructor.
     * @param children pointer to pane root children.
     * @param width minefield cell width.
     * @param height minefield cell height.
     */
    public MinefieldCellView(ObservableList<Node> children, int width, int height)
    {
        fieldRect = new Rectangle(width, height);
        fieldRect.setStroke(Color.GRAY);
        text = new Text();
        children.addAll(fieldRect, text);
    }

    /**
     * Method which is refreshing cell view.
     * @param cell pointer to minefield cell which will be refreshed.
     */
    public void refresh(MinefieldCell cell)
    {
        if(cell.isMarked())
        {
            text.fillProperty().setValue(Color.RED);
        }
        else
        {
            text.fillProperty().setValue(Color.BLACK);
        }

        if(cell.isOpened())
        {
            if(cell.hasBomb())
            {
                fieldRect.fillProperty().setValue(Color.RED);
                text.fillProperty().setValue(Color.WHITE);
            }
            else
            {
                fieldRect.fillProperty().setValue(null);
            }
        }
        else
        {
            fieldRect.fillProperty().setValue(Color.BLACK);
        }

        text.setVisible(cell.isTextVisible());
        text.setText(cell.getTextString());
    }
}
