package com.biskup.minesweeperclient.Views;

import javafx.stage.Stage;

import java.util.List;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class GameView
{
    /**
     * Pointer to game scene
     */
    protected GameScene gameScene;

    /**
     * Pointer to game window stage.
     */
    private Stage stage;

    /**
     * GameView class constructor.
     * @param stage game window stage.
     * @param minefield pointer to list of minefield cell rows representing minefield
     */
    public GameView(Stage stage, List<List<MinefieldCell>> minefield)
    {
        this.stage = stage;
        gameScene = new GameScene(800, 800, stage);
        refreshView(minefield);
    }

    /**
     * Method which is refreshing minefield view.
     * @param minefield pointer to minefield.
     */
    private void refreshView(List<List<MinefieldCell>> minefield)
    {
        if(gameScene.getPaneRoot().getChildren().size() == 0)
        {
            initializePaneRootElements(minefield);
        }
    }

    /**
     * Method which is initializing pane root elements.
     * @param minefield pointer to minefield.
     */
    private void initializePaneRootElements(List<List<MinefieldCell>> minefield)
    {
        for(List<MinefieldCell> row : minefield)
        {
            for(MinefieldCell cell : row)
            {
                gameScene.getPaneRoot().getChildren().add(cell);
            }
        }
    }
}
