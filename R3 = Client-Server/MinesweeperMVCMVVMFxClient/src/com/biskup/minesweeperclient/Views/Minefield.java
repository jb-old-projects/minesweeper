package com.biskup.minesweeperclient.Views;

import com.biskup.minesweeperclient.Controller.ServerCommunication;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jakub Biskup
 * @version 3.0
 */

public class Minefield
{
    /**
     * Pointer to minefield.
     */
    private List<List<MinefieldCell>> minefield;

    /**
     * Minefield X-Axis cell count.
     */
    private int cellCountX;

    /**
     * Minefield Y-Axis cell count.
     */
    private int cellCountY;

    /**
     * Server communication interface.
     */
    private ServerCommunication serverCommunication;

    /**
     * Minefield getter.
     * @return pointer to minefield.
     */
    public List<List<MinefieldCell>> getMinefield()
    {
        return minefield;
    }

    /**
     * Minefield class contructor.
     * @param serverCommunication pointer to server communication interface.
     * @param cellCountX minefield X-Axis cell count.
     * @param cellCountY minefield Y-Axis cell count.
     * @param cellWidth minefield cell width.
     * @param cellHeight minefield cell height.
     */
    public Minefield(ServerCommunication serverCommunication, int cellCountX, int cellCountY, int cellWidth, int cellHeight)
    {
        this.cellCountX = cellCountX;
        this.cellCountY = cellCountY;
        this.serverCommunication = serverCommunication;
        createEmptyMinefield(cellWidth, cellHeight);
    }

    /**
     * Method which is refreshing game view.
     * @param serverResponse server response containing parsed minefield data.
     */
    public void refreshView(String serverResponse)
    {
        String minefieldCellInfos[] = serverResponse.split(";");

        for(int i = 0 ; i< minefield.size(); i++)
        {
            for(int j = 0 ; j < minefield.get(i).size(); j++)
            {
                minefield.get(i).get(j).refreshView(minefieldCellInfos[(i%20)*20 + j]);
            }
        }
    }

    /**
     * Method which is creating empty minefield.
     * @param cellWidth minefield cell width.
     * @param cellHeight minefield cell height.
     */
    private void createEmptyMinefield(int cellWidth, int cellHeight)
    {
        minefield = new ArrayList<>();

        for(int x = 0 ; x < cellCountX ; x++)
        {
            minefield.add(new ArrayList<>());

            for(int y = 0 ; y < cellCountY ; y++)
            {
                MinefieldCell cell = new MinefieldCell(serverCommunication, this, x, y, cellWidth, cellHeight);
                minefield.get(x).add(cell);
            }
        }
    }
}
