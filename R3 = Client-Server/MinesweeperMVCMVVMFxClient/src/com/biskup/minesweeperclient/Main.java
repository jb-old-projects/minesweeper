package com.biskup.minesweeperclient;

import com.biskup.minesweeperclient.Controller.GameClient;
import javafx.application.Application;

/**
 * @author Jakub Biskup
 * @version 3.0
 * Main class of MinesweeperClient application
 */

public class Main
{
    /**
     * Main method from MinesweeperClient application which runs when program starts
     * @param args arguments from command line
     */
    public static void main(String[] args)
    {
        Application.launch(GameClient.class, args);
    }
}
